# Hellom210 CD - Beispiel Kubernetes in Azure mit MySQL

Verwende als Grundlage das "Beispiel Kubernetes in Azure"

Um uns mit der Datenbank in Azure MySQL verbinden zu können müssen wir die Verbindungsdaten speichern und den Pods als Umgebungsvariabeln mitgeben. Wir verwenden dazu Kubernetes Secrets. Wir könnten dazu auch den Azure Key Vault verwenden, da es jedoch mehr darum geht Kubernetes kennenzulernen verwenden wir eben Kubernetes Secrets. Kubernetes Secrets werden nur in Base64 enkodiert abgelegt und bieten nicht diesselbe Sicherheit wie z.B. der Azure Key Vault. Jedoch stellt dies nur ein Problem dar, falls der Kubernetes Cluster kompromitiert wäre. Zu unseren Testzwecken genügt es jedoch und lernt die Techniken um später auf Azure Key Vault umstellen zu können.

Achte darauf wie wir im Deployment die Secrets als Umgebungsvariabeln aus dem Secrets Speicher auslesen und dem Container mitgeben.

Ergänze einfach deine bestehende Konfiguration. Du musst nur die Kubernetes Secrets manuell erstellen und deine deployment.yml anpassen damit die Umgebungsvariabeln in deinen hellom210 Pods mitgegeben werden.

## Die MySQL Funktion in hellom210 aktivieren und testen

- Logge dich in Azure ein und verbinde dich mit Kubernetes. Falls du mehrere Tenants hast gebe den richtigen Tenant noch zusätzlich an.  
  `az login`
  `az login --tenant <UUID>`
- Verbinde dich mit dem Kubernetes. Dies stellt den Kontext von kubectl automatisch auf die K8s-Instanz sodass alle Befehle da ausgeführt werden.  
  `az aks get-credentials --resource-group rgK8s --name aksK8s`

- Den Namespace für die Applikation erstellen und so einstellen, dass wir automatisch in diesem Namespace arbeiten.  
  `kubectl create namespace hellom210`  
  `kubectl config set-context --current --namespace=hellom210`

- Gitlab Container Registry verbinden damit wir das Image pullen können.  
  `kubectl create secret docker-registry gitlab-registry-secret --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<acces-token>`

- Kubernetes Secrets für die Umgebungsvariabeln mit der DB-Konfiguration erstellen.  
  `kubectl create secret generic db-secrets --from-literal=DB_HOST=localhost --from-literal=DB_NAME=hellom210 --from-literal=DB_USERNAME=hellom210user --from-literal=DB_PASSWORD=hellom210password --namespace=hellom210`

- Pods erstellen:
  `kubectl apply -f hellom210-deployment.yml`

- Service erstellen:
  `kubectl apply -f hellom210-service.yml`

- Ingress-Controller erstellen:
  `kubectl apply -f hellom210-ingress.yml`

- K8s Ingress öffentliche IP anzeigen:
  `kubectl get ingress`

- Teste ob du auf die Webapp zugreifen kannst und der Datenbank-Status auf Success und Connected steht.
  `curl http://<minikubeip>`
